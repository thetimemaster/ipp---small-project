#ifndef PARSER_H
#define PARSER_H
#include <inttypes.h>

#define EOF_LINE 0
#define WRONG_LINE_EOF 1
#define WRONG_LINE 2
#define MALLOC_ERROR 3

#define EMPTY_LINE 4
#define COMMENT_LINE 5

#define C_VALID 6
#define C_REMOVE 7
#define C_DECLARE 8
#define C_SET_ENERGY 9
#define C_GET_ENERGY 10
#define C_EQUAL 11
#define OK 12

#define COMMAND_MAX_LEN 16

typedef struct ParsedLine
{
	unsigned lineType;
	char* historyA;
	char* historyB;
	uint64_t energy;
} ParsedLine;

ParsedLine* NextLine(ParsedLine* line, char* command);

#endif
