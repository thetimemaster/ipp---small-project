#ifndef ENERGY_CLASS_H
#define ENERGY_CLASS_H
#include <inttypes.h>

typedef struct EnergyClass
{
	uint64_t energy;
	unsigned pointerCount;
	struct EnergyClass* parentClass;
} EnergyClass;

EnergyClass* NewEnergyClass();

void RemoveEnergyPointer(EnergyClass* energyClass);

uint64_t GetEnergy(EnergyClass* energyClass);

void SetEnergy(EnergyClass* energyClass, uint64_t energy);

void MergeEnergyClasses(EnergyClass* energyClassA, EnergyClass* energyClassB);

#endif
