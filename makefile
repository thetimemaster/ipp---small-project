FILES = parser trie energyClass
MAIN = quantization

CC = gcc
CFLAGS = -Wall -Wextra -std=c11 -O2

all: $(FILES:%=%.o) $(MAIN).o
	$(CC) $(CFLAGS) $^ -o $(MAIN)

.c.o: $(FILES:%=%.h)
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	@rm -f $(FILES:%=%.o) $(MAIN).o $(MAIN)
