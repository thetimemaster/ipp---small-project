#!/bin/bash
stacksize=65528

ulimit -s "${stacksize}"
for f in "$2"/*.in
do
	echo Running on "$f"
	if ./"$1" < "$f" > "${f%.in}.myout" 2> "${f%.in}.myerr"
	then
		echo -e "> \e[32mRUN SUCCESSFUL\e[39m"

		if [ -f "${f%.in}.out" ]
		then
			if diff "${f%.in}.myout" "${f%.in}.out" > /dev/null
				then echo -e "> \e[32mSTDOUT OK\e[39m"
				else echo -e "> \e[91mSTDOUT DIFFER\e[39m"
			fi
		else echo -e "  \e[33m${f%.in}.out DOES NOT EXIST\e[39m"
		fi

		if [ -f "${f%.in}.err" ]
		then
			if diff "${f%.in}.myerr" "${f%.in}.err" > /dev/null
				then echo -e "> \e[32mSTDERR OK\e[39m"
				else echo -e "> \e[91mSTDERR DIFFER\e[39m"
			fi
		else echo -e "> \e[33m${f%.in}.err DOES NOT EXIST\e[39m"
		fi

	else echo -e "> \e[91mRUN FAILED\e[39m"
	fi

	if valgrind --main-stacksize="${stacksize}000" -q --error-exitcode=1 \
		--leak-check=full ./"$1" < "$f" &> /dev/null
	then echo -e "> \e[32mNO LEAKS FOUND\e[39m"
	else echo -e "> \e[91mMEMORY LEAK DETECTED\e[39m"
	fi

	rm -f "${f%.in}.myout" "${f%.in}.myerr"
done
