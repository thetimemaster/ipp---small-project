#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "parser.h"
#include "trie.h"
#include "energyClass.h"

static void FreeParsedLine(ParsedLine* line)
{
	free(line -> historyA);
	free(line -> historyB);
	free(line);
}

//Frees Trie, command buffer and ParsedLine in one command
static int FreeAll(TrieNode* trie, char* buffer, ParsedLine* line)
{
	FreeSubtree(trie);
	free(buffer);
	FreeParsedLine(line);
	return 1;
}

//Command VALID from the task
static void CommandValid(char* history, TrieNode* trieRoot)
{
	TrieNode* trieNode = FindHistory(trieRoot, history, 0);
	if(trieNode == NULL) printf("NO\n");
	else printf("YES\n");
}

//Command REMOVE from the task
static void CommandRemove(char* history, TrieNode* trieRoot)
{
	DisallowHistory(trieRoot, history, 0);
	printf("OK\n");
}

//Command DECLARE from the task
static unsigned CommandDeclare(char* history, TrieNode* trieRoot)
{
	unsigned code = AllowHistory(trieRoot, history, 0);
	if(code == MALLOC_ERROR) return MALLOC_ERROR;
	printf("OK\n");
	return OK;
}

//One parameter command ENERGY from the task
static void CommandGetEnergy(char* history, TrieNode* trieRoot)
{
	TrieNode* trieNode = FindHistory(trieRoot, history, 0);

	if(trieNode != NULL)
	{
		uint64_t energy = GetEnergy(trieNode -> energyClass);
		if(energy != 0) printf("%" PRIu64 "\n", energy);
		else fprintf(stderr, "ERROR\n");
	}
	else fprintf(stderr, "ERROR\n");
}

//Two parameter command ENERGY from the task
static unsigned CommandSetEnergy(char* history, uint64_t energy, TrieNode* trieRoot)
{
	TrieNode* trieNode = FindHistory(trieRoot, history, 0);

	if(trieNode != NULL)
	{
		if(trieNode -> energyClass == NULL)
		{
			EnergyClass* energyClass = NewEnergyClass();
			if(energyClass == NULL) return MALLOC_ERROR;

			energyClass -> pointerCount++;
			trieNode -> energyClass = energyClass;
		}
		SetEnergy(trieNode -> energyClass, energy);
		printf("OK\n");
	}
	else fprintf(stderr, "ERROR\n");
	return OK;
}

//Command EQUAL from the task
static void CommandEqual(char* historyA, char* historyB, TrieNode* trieRoot)
{
	TrieNode* nodeA = FindHistory(trieRoot, historyA, 0);
	TrieNode* nodeB = FindHistory(trieRoot, historyB, 0);

	if(nodeA == nodeB && nodeA != NULL)
	{
		printf("OK\n");
		return;
	}

	if(nodeA != NULL && nodeB != NULL)
	{
		if(nodeA -> energyClass == NULL)
		{
			TrieNode* nodeC = nodeA;
			nodeA = nodeB;
			nodeB = nodeC;
		}
		if(nodeA -> energyClass != NULL)
		{
			if(nodeB -> energyClass == NULL)
			{
				nodeA -> energyClass -> pointerCount++;
				nodeB -> energyClass = nodeA -> energyClass;
			}
			else
				MergeEnergyClasses(nodeA -> energyClass, nodeB -> energyClass);
			printf("OK\n");
		}
		else fprintf(stderr, "ERROR\n");
	}
	else fprintf(stderr, "ERROR\n");
}

int main()
{
	char* command = malloc(sizeof(char) * COMMAND_MAX_LEN);
	ParsedLine* line = malloc(sizeof(ParsedLine));
	TrieNode* trieRoot = NewTrieNode();

	if(command == NULL || trieRoot == NULL || line == NULL) return 1;

	while(true)
	{
		line = NextLine(line, command);
		fflush(stdout);
		if(line -> lineType == MALLOC_ERROR) return FreeAll(trieRoot, command, line);

		if(line -> lineType == WRONG_LINE) fprintf(stderr, "ERROR\n");
		if(line -> lineType == WRONG_LINE_EOF) fprintf(stderr, "ERROR\n");

		if(line -> lineType == C_VALID) CommandValid(line -> historyA, trieRoot);

		if(line -> lineType == C_REMOVE) CommandRemove(line -> historyA, trieRoot);

		if(line -> lineType == C_DECLARE)
			if(CommandDeclare(line -> historyA, trieRoot) == MALLOC_ERROR)
				return FreeAll(trieRoot, command, line);

		if(line -> lineType == C_GET_ENERGY) CommandGetEnergy(line -> historyA, trieRoot);

		if(line -> lineType == C_SET_ENERGY)
			if(CommandSetEnergy(line -> historyA, line -> energy, trieRoot) == MALLOC_ERROR)
				return FreeAll(trieRoot, command, line);

		if(line -> lineType == C_EQUAL) CommandEqual(line -> historyA, line -> historyB, trieRoot);

		free(line -> historyA);
		free(line -> historyB);

		if(line -> lineType == WRONG_LINE_EOF) break;
		if(line -> lineType == EOF_LINE) break;
	}

	return FreeAll(trieRoot, command, line) - 1;
}
