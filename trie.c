#include <stdlib.h>
#include "energyClass.h"
#include "trie.h"
#include "parser.h"

//Resursively frees Trie tree and EnergyClass pointers
void FreeSubtree(TrieNode* node)
{
	if(node == NULL) return;

	RemoveEnergyPointer(node -> energyClass);
	for(int i = 0; i < 4; i++) FreeSubtree(node -> edges[i]);
	free(node);
}

//Returns pointer to new TrieNode or NULL in case of malloc error
TrieNode* NewTrieNode()
{
	TrieNode* o = malloc(sizeof(TrieNode));
	if(o != NULL)
	{
		o -> energyClass = NULL;
		for(int i = 0; i < 4; i++) o -> edges[i] = NULL;
	}
	return o;
}

//Resursively adds new history to Trie
unsigned AllowHistory(TrieNode* node, char* buffer, unsigned pos)
{
	if(buffer[pos] == '\0') return OK;

	if(node -> edges[buffer[pos] - '0'] == NULL)
	{
		TrieNode* trieNode = NewTrieNode();
		if(trieNode == NULL) return MALLOC_ERROR;
		node -> edges[buffer[pos] - '0'] = trieNode;
	}
	return AllowHistory(node -> edges[buffer[pos] - '0'], buffer, pos + 1);
}

//Recursively removes history from trie
unsigned DisallowHistory(TrieNode* node, char* buffer, unsigned pos)
{
	const unsigned DISALLOW_OK = 0;
	const unsigned DISALLOW_FREE = 1;

	if(buffer[pos] == '\0')
	{
		FreeSubtree(node);
		return DISALLOW_FREE;
	}
	if(node -> edges[buffer[pos] - '0'] == NULL) return DISALLOW_OK;
	if(DisallowHistory(node -> edges[buffer[pos] - '0'], buffer, pos + 1) == DISALLOW_FREE)
	{
		node -> edges[buffer[pos] - '0'] = NULL;
	}
	return DISALLOW_OK;
}

//Finds node corresponding to given history in Trie or returns NULL if not found
TrieNode* FindHistory(TrieNode* node, char* buffer, unsigned pos)
{
	if(buffer[pos] == '\0') return node;
	if(node -> edges[buffer[pos] - '0'] == NULL) return NULL;
	return FindHistory(node -> edges[buffer[pos] - '0'], buffer, pos + 1);
}
