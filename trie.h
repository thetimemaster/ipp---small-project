#ifndef TRIE_H
#define TRIE_H
#include "energyClass.h"

typedef struct TrieNode
{
	struct TrieNode* edges[4];
	EnergyClass* energyClass;
} TrieNode;

void FreeSubtree(TrieNode* node);

TrieNode* NewTrieNode();

unsigned AllowHistory(TrieNode* node, char* buffer, unsigned pos);

unsigned DisallowHistory(TrieNode* node, char* buffer, unsigned pos);

TrieNode* FindHistory(TrieNode* node, char* buffer, unsigned pos);

#endif
