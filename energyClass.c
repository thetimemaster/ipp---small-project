#include <stdlib.h>
#include "energyClass.h"

//Returns pointer to new EnergyClas or NULL in case of malloc error
EnergyClass* NewEnergyClass()
{
	EnergyClass* energyClass = malloc(sizeof(EnergyClass));
	if(energyClass == NULL) return NULL;

	energyClass -> energy = 0;
	energyClass -> pointerCount = 0;
	energyClass -> parentClass = NULL;

	return energyClass;
}

//Removes pointer to EnergyClass and recursively frees it
//if that was a last pointer
void RemoveEnergyPointer(EnergyClass* energyClass)
{
	if(energyClass == NULL) return;

	energyClass -> pointerCount--;

	if(energyClass -> pointerCount == 0)
	{
		RemoveEnergyPointer(energyClass -> parentClass);
		free(energyClass);
	}
}

//Finds root of element in F&U
static EnergyClass* GetRootEnergyClass(EnergyClass* energyClass)
{
	if(energyClass -> parentClass == NULL) return energyClass;

	EnergyClass* newParentClass = GetRootEnergyClass(energyClass -> parentClass);

	if(newParentClass != energyClass -> parentClass)
	{
		newParentClass -> pointerCount++;
		RemoveEnergyPointer(energyClass -> parentClass);
		energyClass -> parentClass = newParentClass;
	}
	return energyClass -> parentClass;
}

//Returns proper energy of root class of given EnergyClass or 0
//if the parameter is NULL
uint64_t GetEnergy(EnergyClass* energyClass)
{
	if(energyClass == NULL) return 0;

	EnergyClass* rootClass = GetRootEnergyClass(energyClass);
	return rootClass -> energy;
}

//Sets energy of root class of given EnergyClass
void SetEnergy(EnergyClass* energyClass, uint64_t energy)
{
	energyClass = GetRootEnergyClass(energyClass);
	if(energyClass == NULL) return;

	energyClass -> energy = energy;
}

//Unitest two energy classes in a equity relationship
void MergeEnergyClasses(EnergyClass* energyClassA, EnergyClass* energyClassB)
{
	EnergyClass* rootClassA = GetRootEnergyClass(energyClassA);
	EnergyClass* rootClassB = GetRootEnergyClass(energyClassB);

	if(rootClassA != rootClassB)
	{
		rootClassA -> parentClass = rootClassB;
		rootClassB -> pointerCount++;

		uint64_t add = 0;
		if(rootClassA -> energy % 2 == 1 && rootClassB -> energy % 2 == 1) add = 1;
		rootClassB -> energy = rootClassA -> energy / 2 + rootClassB -> energy / 2 + add;
	}
}

