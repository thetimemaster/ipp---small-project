#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "parser.h"

//Returns NULL or proper history read while informing abou EOF/EOL or malloc error
static char* HistoryParameter(unsigned* eofSignal, unsigned* eolSignal, unsigned* errorSignal)
{
	*eofSignal = *eolSignal = *errorSignal = false;
	unsigned len = 0;
	unsigned size = 1;
	char* buffer = malloc(sizeof(char) * size);
	if(buffer == NULL)
	{
		*errorSignal = true;
		return NULL;
	}

	while(true)
	{
		int c = getchar();

		if(c == EOF)
		{
			*eofSignal = true;
			free(buffer);
			return NULL;
		}

		if(c == '\n' || c == ' ')
		{
			if(c == '\n') *eolSignal = true;

			if(len == 0)
			{
				free(buffer);
				return NULL;
			}
			buffer[len] = '\0';
			return buffer;
		}

		if(c < '0' || c > '3')
		{
			free(buffer);
			return NULL;
		}

		buffer[len] = c;
		len++;

		if(len == size)
		{
			char* newBuffer = malloc(sizeof(char) * size * 2);

			if(newBuffer == NULL)
			{
				free(buffer);
				*errorSignal = true;
				return NULL;
			}

			for(unsigned i = 0; i < size; i++) newBuffer[i] = buffer[i];
			size *= 2;

			free(buffer);
			buffer = newBuffer;
		}
	}
}

//Returns 0 or proper energy read while informing about EOF/EOL
static uint64_t EnergyParameter(unsigned* eofSignal, unsigned* eolSignal)
{
	uint64_t output = 0;
	int c;
	*eofSignal = false;
	*eolSignal = false;

	while(true)
	{
		c = getchar();
		if(c == '\n')
		{
			*eolSignal = true;
			return output;
		}
		if(c == ' ') return output;
		if(c == '\n')
		{
			*eofSignal = true;
			return output;
		}
		if(c < '0' || c > '9') return 0;

		if(output > (UINT64_MAX - (uint64_t)c + '0') / 10) return 0;
		output = output * 10 + c - '0';
	}
}

//Fills and returns given ParsedLine with parameters in one command
static ParsedLine* FillParsedLine(ParsedLine* line, unsigned type, char* h1, char* h2, uint64_t energy)
{
	line -> lineType = type;
	line -> historyA = h1;
	line -> historyB = h2;
	line -> energy = energy;
	return line;
}

//Fills and returns given ParsedLine type without any important parameters
static ParsedLine* ParsedLineNoPar(ParsedLine* line, unsigned type)
{
	return FillParsedLine(line, type, NULL, NULL, 0);
}

//Reads wrong line to nearest EOF/EOL and fills and returns ParsedLine with corresponding message
static ParsedLine* EndWrongLine(ParsedLine* line)
{
	int c = 1;
	while(c != '\n' && c != EOF) c = getchar();

	if(c == '\n') return ParsedLineNoPar(line, WRONG_LINE);
	return ParsedLineNoPar(line, WRONG_LINE_EOF);
}

//Reads wrong line to nearest EOF/EOL and fills and returns ParsedLine with corresponding message
static ParsedLine* EndCommentLine(ParsedLine* line)
{
	int c = 1;
	while(c != '\n' && c != EOF) c = getchar();

	if(c == '\n') return ParsedLineNoPar(line, COMMENT_LINE);
	return ParsedLineNoPar(line, WRONG_LINE_EOF);
}

//Reads new line and fills and returns ParsedLine given with parameters
ParsedLine* NextLine(ParsedLine* line, char* command)
{
	unsigned eofSignal = false, eolSignal = false, errorSignal = false;
	unsigned len = 1;

	int c = getchar();

	if(c == EOF) return ParsedLineNoPar(line, EOF_LINE);
	if(c == '\n') return ParsedLineNoPar(line, EMPTY_LINE);
	if(c == '#') return EndCommentLine(line);
	if(c < 'A' || c > 'Z') return EndWrongLine(line);
	command[0] = c;

	while(len < COMMAND_MAX_LEN)
	{
		c = getchar();
		if(c == EOF) return ParsedLineNoPar(line, WRONG_LINE_EOF);
		if(c == '\n') return ParsedLineNoPar(line, WRONG_LINE);
		if(c == ' ') break;
		if(c < 'A' || c > 'Z') return EndWrongLine(line);
		command[len] = c;
		len++;
	}

	if(len == COMMAND_MAX_LEN) return EndWrongLine(line);

	command[len] = '\0';

	if(strcmp(command, "VALID") == 0 || strcmp(command, "REMOVE") == 0 || strcmp(command, "DECLARE") == 0)
	{
		char* history = HistoryParameter(&eofSignal, &eolSignal, &errorSignal);

		if(errorSignal) return ParsedLineNoPar(line, MALLOC_ERROR);
		if(history == NULL)
		{
			if(eolSignal) return ParsedLineNoPar(line, WRONG_LINE);
			if(eofSignal) return ParsedLineNoPar(line, WRONG_LINE_EOF);
			return EndWrongLine(line);
		}
		if(!eolSignal)
		{
			free(history);
			return EndWrongLine(line);
		}

		if(strcmp(command, "VALID") == 0) return FillParsedLine(line, C_VALID, history, NULL, 0);
		if(strcmp(command, "REMOVE") == 0) return FillParsedLine(line, C_REMOVE, history, NULL, 0);
		if(strcmp(command, "DECLARE") == 0) return FillParsedLine(line, C_DECLARE, history, NULL, 0);
	}

	if(strcmp(command, "ENERGY") == 0)
	{
		char* history = HistoryParameter(&eofSignal, &eolSignal, &errorSignal);

		if(errorSignal) return ParsedLineNoPar(line, MALLOC_ERROR);
		if(history == NULL)
		{
			if(eolSignal) return ParsedLineNoPar(line, WRONG_LINE);
			if(eofSignal) return ParsedLineNoPar(line, WRONG_LINE_EOF);
			return EndWrongLine(line);
		}

		if(eolSignal) return FillParsedLine(line, C_GET_ENERGY, history, NULL, 0);

		uint64_t energy = EnergyParameter(&eofSignal, &eolSignal);

		if(energy == 0)
		{
			free(history);
			if(eolSignal) return ParsedLineNoPar(line, WRONG_LINE);
			if(eofSignal) return ParsedLineNoPar(line, WRONG_LINE_EOF);
			return EndWrongLine(line);
		}
		if(!eolSignal)
		{
			free(history);
			return EndWrongLine(line);
		}

		return FillParsedLine(line, C_SET_ENERGY, history, NULL, energy);
	}

	if(strcmp(command, "EQUAL") == 0)
	{
		char* historyA = HistoryParameter(&eofSignal, &eolSignal, &errorSignal);

		if(errorSignal) return ParsedLineNoPar(line, MALLOC_ERROR);
		if(historyA == NULL)
		{
			if(eolSignal) return ParsedLineNoPar(line, WRONG_LINE);
			if(eofSignal) return ParsedLineNoPar(line, WRONG_LINE_EOF);
			return EndWrongLine(line);
		}
		if(eolSignal)
		{
			free(historyA);
			return ParsedLineNoPar(line, WRONG_LINE);
		}

		char* historyB = HistoryParameter(&eofSignal, &eolSignal, &errorSignal);

		if(errorSignal)
		{
			free(historyA);
			return ParsedLineNoPar(line, MALLOC_ERROR);
		}
		if(historyB == NULL)
		{
			free(historyA);
			if(eolSignal) return ParsedLineNoPar(line, WRONG_LINE);
			if(eofSignal) return ParsedLineNoPar(line, WRONG_LINE_EOF);
			return EndWrongLine(line);
		}
		if(!eolSignal)
		{
			free(historyA);
			free(historyB);
			return EndWrongLine(line);
		}

		return FillParsedLine(line, C_EQUAL, historyA, historyB, 0);
	}

	return EndWrongLine(line);
}
